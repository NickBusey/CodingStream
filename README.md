# Nick Busey Twitch Stream Assets

I believe in Open Source, and try to release as much as I can in Open Source format.

On my [Twitch stream](https://www.twitch.tv/nickbusey) I work exclusively on open source software,
I thought it only made sense to release what drives the stream itself as open source assets as well.

I have so far included my OBS Scene export, and my blender assets.

## Financials

I will be publishing data on all my financials from my open source work.
So all Twitch, YouTube, Patreon, etc. revenue I will make available in
CSV format, and provide Jupyter Notebooks to visualize and manipulate the data.

## Jupyter Notebooks

`docker run --rm -p 8888:8888 -v "$PWD":/home/jovyan jupyter/datascience-notebook`